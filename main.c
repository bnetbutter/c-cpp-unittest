#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "unittest.h"

typedef struct {
    const DataObject _;
    struct Point {
        int x, y;
    } data;
} Point;

static int
Point_stringize(const struct Point *p, char buffer[], size_t size)
{
    return snprintf(buffer, size, "Point(%d, %d)", p->x, p->y);
}

static int
Point_compare_eq(const struct Point *a, const struct Point *b)
{
    return a->x == b->x && a->y == b->y;
}

static int
Point_compare_neq(const struct Point *a, const struct Point *b)
{
    return !(a->x == b->x && a->y == b->y);
}

static void
Point_init(struct Point *self, va_list *app)
{
    self->x = va_arg(*app, int);
    self->y = va_arg(*app, int);
}



const DataClass PointClass = {
    .size=sizeof(Point),
    .init=Point_init,
    .stringize=Point_stringize,
    .compare_eq=Point_compare_eq,
    .compare_neq=Point_compare_neq,
};

#define Point(x,y)  (testdata_create(&PointClass, x, y))

int
main()
{
    assertion(Integer(1), !=, Integer(1));
    assertion(Integer(3), <, Integer(2)); 
    assertion(String("Hello"), ==, String("World!"));
    assertion(String("Hello"), ==, String("Hello"));
    assertion(Point(2, 3), !=, Point(2, 3), "HELLOOOOOOOOO");

    return test_result();
}