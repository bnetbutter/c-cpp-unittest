#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <assert.h>

#include <sys/wait.h>
#include "unittest.h"


typedef enum {
    Eq,
    nEq,
    Lt,
    LtE,
    Gt,
    GtE,
    In,
    NotIn,
} Comparators;

static TestResult *head;
static TestResult *last;

static TestResult *
TestResult_create()
{
    TestResult *node = calloc(1, sizeof(TestResult));
    if (! head) {
        head = last = node;
    }
    last->next = node;
    last = node;
    return node;
}

static int
stringize_data(const DataObject *self, char buffer[], size_t size)
{
    memset(buffer, 0, size);
    return self->cls->stringize(self + 1, buffer, size);
}


static struct {
    char **linebuf;
    int *nchars;
    int nlines;
} linebuf;


static int
count_chars_in_current_line(FILE *fp)
{
    int i = 0;
    char c;
    while (! feof(fp)) {
        i++;
        if (c = fgetc(fp) == '\n') {
            break;
        }
    }
    
    return i;
}

static int
count_number_of_lines(FILE *fp)
{
    char c;
    int i = 0;
    while (! feof(fp)) {
        if (fgetc(fp) == '\n') {
            i++;
        }
    }
    fseek(fp, -1, SEEK_END);
    if (fgetc(fp) != '\n') {
        i++;
    }
    rewind(fp);
    return i;
}

static void
readline(FILE *fp, char buffer[], int size)
{
    for (int i = 0; i < size; i++) {
        buffer[i] = fgetc(fp);
    }
    buffer[size - 1] = 0;
}

static void
init_linebuffer(FILE *fp)
{
    
    linebuf.nlines = count_number_of_lines(fp);
    linebuf.nchars = calloc(linebuf.nlines, sizeof(int));

    linebuf.linebuf = calloc(linebuf.nlines, sizeof(char *));
 
    for (int i = 0; i < linebuf.nlines; i++) {
        int count = count_chars_in_current_line(fp);
        linebuf.linebuf[i] = malloc(count + 1); 
        linebuf.nchars[i] = count;
    }
    rewind(fp);
    for (int i = 0; i < linebuf.nlines; i++) {
        readline(fp, linebuf.linebuf[i], linebuf.nchars[i]);
    }
}

int 
_test_result(const char *file)
{
    FILE *fp = fopen(file, "r");
    if (! fp) {
        perror("fopen");
        return -1;
    }
    init_linebuffer(fp);
    fclose(fp);

    int res;    
    if (! (res = _get_test_result(head))) {
        TestResult_print();
    }
    return res;
}

static const char *
operator_negation(const char *operator)
{
    if (strcmp(operator, "==") == 0) {
        return "!=";
    }
    else if (strcmp(operator, "!=") == 0) {
        return "==";
    }
    else if (strcmp(operator, "<") == 0) {
        return ">=";
    }
    else if (strcmp(operator, "<=") == 0) {
        return ">";
    }
    else if (strcmp(operator, ">") == 0) {
        return "<=";
    }
    else if (strcmp(operator, ">=") == 0) {
        return "<";
    }
    else {
        errno = EINVAL;
        return NULL;
    }
}


static const char * OK = "\033[36mOK\033[0m";
static const char * FAILED = "\033[31mFAILED\033[0m";

static void
_TestResult_print(const TestResult *testresult, char var_buffer[], size_t size)
{
    if (! testresult) {
        return; 
    }
    
    int assertion_error = false;
    int signaled_error = false;
    if (testresult->result) {
        printf("%s - %s:%d\n", OK, testresult->filename, testresult->lineno);
    }
    else {
        printf("%s - %s:%d\n", FAILED, testresult->filename, testresult->lineno);
    }


    printf(">%s\n", linebuf.linebuf[testresult->lineno - 1]);

    if (! testresult->result) {
        printf("\nAssertionError: ");
        stringize_data(testresult->a, var_buffer, 1024);
        printf("%s %s", var_buffer, operator_negation(testresult->operator));
        stringize_data(testresult->b, var_buffer, 1024);
        printf(" %s", var_buffer);
    }
    if (! testresult->result && strlen(testresult->failure_message)) {
        printf(": \033[33m%s\033[0m\n", testresult->failure_message);
    }
    else {
        printf("\n");
    }
    
    for (int i = 0; i < 40; i++) {
        printf("-");
    } printf("\n");
    _TestResult_print(testresult->next, var_buffer, size);
}


int 
_get_test_result(TestResult *result)
{
    if (!result) {
        return true;
    }
    return result->result &&  _get_test_result(result->next);
}

void
TestResult_print()
{
    char buffer[1024];
    _TestResult_print(head, buffer, sizeof(buffer));
}

typedef int (*Compare)(const void *, const void *);

static Compare
get_comparator(const DataClass *cls, const char *op)
{
    if (strcmp(op, "==") == 0) {
        return cls->compare_eq;
    }
    else if (strcmp(op, "!=") == 0) {
        return cls->compare_neq;
    }
    else if (strcmp(op, ">") == 0) {
        return cls->compare_gt;
    }
    else if (strcmp(op, ">=") == 0) {
        return cls->compare_gte;
    }
    else if (strcmp(op, "<") == 0) {
        return cls->compare_lt;
    }
    else if (strcmp(op, "<=") == 0) {
        return cls->compare_lte;
    }
    else {
        errno = EINVAL;
        return NULL;
    }
}

void _assertion(
    const DataObject *a,
    const char *operator,
    const DataObject *b,
    const char *message,
    const char *_filename,
    int _lineno)
{
    TestResult *result = TestResult_create();
    result->a = a;
    result->operator = operator;
    result->b = b;
    result->failure_message = message;
    result->filename = _filename;
    result->lineno = _lineno;
    
    Compare fn = get_comparator(a->cls, operator);
    if (! fn) {
        perror(operator);
        exit(1);
    }
    
    result->result = fn(a + 1, b + 1);
}

void *
testdata_create(const DataClass *cls, ...)
{
    va_list ap;
    va_start(ap, cls);
    
    DataObject *data = calloc(1, cls->size);
    *(DataClass **) data = cls;
    if (cls->init) {
        cls->init(data + 1, &ap);
    }
    va_end(ap);
    return data;
}

static void
integer_init(long *self, va_list *app)
{
    *self = va_arg(*app, long);    
}

static int
integer_compare_eq(const long *a, const long *b)
{
    return *a == *b;
}

static int
integer_compare_neq(const long *a, const long *b)
{
    return *a != *b;
}

static int
integer_compare_gt(const long *a, const long *b)
{
    return *a > *b;
}

static int
integer_compare_gte(const long *a, const long *b)
{
    return *a >= *b;
}

static int
integer_compare_lt(const long *a, const long *b)
{
    return *a < *b;
}

static int
integer_compare_lte(const long *a, const long *b)
{
    return *a <= *b;
}

static int
integer_stringize(const long *self, char buffer[], size_t size)
{
    return snprintf(buffer, size, "%ld", *self);
}

const DataClass IntegerClass = {
    .size=sizeof(Integer),
    .init=integer_init,
    .stringize=integer_stringize,
    .compare_eq=integer_compare_eq,
    .compare_neq=integer_compare_neq,
    .compare_lt=integer_compare_lt,
    .compare_lte=integer_compare_lte,
    .compare_gt=integer_compare_gt,
    .compare_gte=integer_compare_gte
};

static int
double_compare_eq(const double *a, const double *b)
{
    return *a == *b;
}

static int
double_compare_neq(const double *a, const double *b)
{
    return *a != *b;
}

static int
double_compare_gt(const double *a, const double *b)
{
    return *a > *b;
}

static int
double_compare_gte(const double *a, const double *b)
{
    return *a >= *b;
}

static int
double_compare_lt(const double *a, const double *b)
{
    return *a < *b;
}

static int
double_compare_lte(const double *a, const double *b)
{
    return *a <= *b;
}

static int
double_stringize(const double *a, char buffer[], size_t size)
{  
    return snprintf(buffer, size, "%lf\n", *a);
}

static void
double_init(double *self , va_list *app)
{
    *self = va_arg(*app, double);
}

const DataClass FloatClass = {
    .size=sizeof(Float),
    .init=double_init,
    .stringize=double_stringize,
    .compare_eq=double_compare_eq,
    .compare_neq=double_compare_neq,
    .compare_lt=double_compare_lt,
    .compare_lte=double_compare_lte,
    .compare_gt=double_compare_gt,
    .compare_gte=double_compare_gte,
};


static void
string_init(char **self, va_list *app)
{
    const char *text = va_arg(*app, const char *);
    *self = malloc(strlen(text) + 1);
    memcpy(*self, text, strlen(text));
}

static int
string_stringize(const char **str, char buffer[], size_t size)
{
    return snprintf(buffer, size, "\"%s\"", *str);
}

static int
string_compare_eq(const char **a, const char **b)
{
    return strcmp(*a, *b) == 0;
}

static int
string_compare_neq(const char **a, const char **b)
{
    return strcmp(*a, *b) != 0;
}

const DataClass StringClass = {
    .size=sizeof(String),
    .stringize=string_stringize,
    .init=string_init,
    .compare_eq=string_compare_eq,
    .compare_neq=string_compare_neq,
};


