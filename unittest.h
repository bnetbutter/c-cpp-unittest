#ifndef UNITTEST_H
#define UNITTEST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>


typedef struct {
    size_t size;
    void (*init)(void *self, va_list *app);
    int (*stringize)(const void *self, char [], size_t);
    int (*compare_eq)(const void * a, const void * b);
    int (*compare_neq)(const void * a, const void * b);
    int (*compare_lt)(const void * a, const void * b);
    int (*compare_lte)(const void * a, const void * b);
    int (*compare_gt)(const void * a, const void * b);
    int (*compare_gte)(const void * a, const void * b);
} DataClass;

typedef struct {
    const DataClass * cls;
} DataObject;

typedef struct TestResult {
    const DataObject * a;
    const DataObject * b;
    const char * filename;
    int lineno;

    const char * operator;
    const char * failure_message;

    int result;
    struct TestResult *next;
} TestResult;

void _assertion(
    const DataObject *a,
    const char *operator,
    const DataObject *b,
    const char *message,
    const char *_filename,
    int _lineno
);

#define _OPTIONAL_MESSAGE(...) ((0, #__VA_ARGS__) ? __VA_ARGS__ : 0)

#define assertion(left, op, right, ...)     (_assertion(left, #op, right, \
    _OPTIONAL_MESSAGE(__VA_ARGS__), __FILE__, __LINE__))


void generate_report();
int _test_result(const char * file);

#define test_result()       (_test_result(__FILE__))

extern const DataClass IntegerClass;
extern const DataClass StringClass;
extern const DataClass FloatClass;

typedef struct {
    const DataObject _;
    long data;
} Integer;

typedef struct {
    const DataObject _;
    char *data;
} String;

typedef struct {
    const DataObject _;
    double data;
} Float;

#define Integer(n)      (testdata_create(&IntegerClass, n))
#define Float(n)        (testdata_create(&FloatClass, n))
#define String(n)       (testdata_create(&StringClass, n))

void * testdata_create(const DataClass *, ...);
void TestResult_print();


#ifdef __cplusplus
}
#endif
#endif